// start BT1
let printTo100 = () => {
  var contentHTML = "";
  for (var i = 0; i <= 9; i++) {
    for (var j = 1; j <= 10; j++) {
      if (j == 10 && i != 0) {
        contentHTML = contentHTML + j * (i + 1);
      } else if (j == 10 && i == 0) {
        contentHTML = contentHTML + "10";
      } else {
        contentHTML = contentHTML + i + j + ` `;
      }
    }
    contentHTML = contentHTML + `<br/>`;
  }
  document.getElementById("printTo100").innerHTML = contentHTML;
};
// end BT1

// start BT2
let arrayBT2 = [];
// render mảng ra màn hinh
let render = () => {
  var contentHTML = "";
  arrayBT2.forEach((item) => {
    contentHTML = contentHTML + " " + item;
  });
  document.getElementById("arrayBT2").innerHTML = contentHTML;
};
// validate dữ liệu đầu vào
let validate = (n) => {
  var isValid = Number.isInteger(n);
  // ktra rỗng
  if (document.getElementById("n").value == "") {
    alert("Vui lòng nhập số n");
    return false;
  }
  // Ktra số nguyên
  if (isValid == false) {
    alert("Chỉ nhập số nguyên. Vui lòng nhập lại.");
    return false;
  } else {
    return true;
  }
};
// thêm n vào mảng
let add = () => {
  var n = document.getElementById("n").value * 1;
  var isValid = validate(n);
  if (isValid) {
    arrayBT2.push(n);
    render();
  }
};
// trả về mảng rỗng
let reset = () => {
  arrayBT2 = [];
  render();
  document.getElementById("listPrimeNumber").innerHTML = "";
};
// xác định số nguyên tố
let primeNumber = (n) => {
  // quy ước: true=số nguyên tố, false=ko số nguyên tố
  if (n < 2) {
    return false;
  } else if (n == 2 || n == 3) {
    return true;
  } else {
    for (var i = 2; i <= Math.sqrt(n); i++) {
      if (n % i == 0) {
        return false;
      }
    }
  }
  return true;
};
// tìm danh sách số nguyên tố
let listPrimeNumber = () => {
  var contentHTML = "";
  arrayBT2.forEach((item) => {
    if (primeNumber(item)) {
      contentHTML = contentHTML + " " + item;
    }
  });
  if (contentHTML == "") {
    document.getElementById("listPrimeNumber").innerHTML =
      "Không có số nguyên tố nào";
  } else {
    document.getElementById(
      "listPrimeNumber"
    ).innerHTML = `Số nguyên tố gồm: <br/>${contentHTML}`;
  }
};
// end BT2

// start BT3
let tinhS = () => {
  var S = 0;
  var n = document.getElementById("n2").value * 1;
  if (n >= 2 && Number.isInteger(n)) {
    for (var i = 2; i <= n; i++) {
      S = S + i;
    }
    S = S + 2 * n;
    document.getElementById("tinhS").innerHTML = S;
  } else alert("N cần là số nguyên và lớn hơn 2. Vui lòng nhập lại.");
};
// end BT3

// start BT4
let timUoc = () => {
  let n = document.getElementById("n4").value;
  let arr=[];
  let regex = /^\d+$/;
  if (regex.test(n) == false) {
    document.getElementById("timUoc").innerHTML = `
                    <div>   
                            Định nghĩa: "Khái niệm ước số: Ước số của một số tự nhiên a là b khi a chia hết cho b. Ví dụ: 9 chia hết cho 3 thì 3 là ước số của 9." 
                            <br/><span class="text-danger font-weight-bold"> Vui lòng nhập lại một SỐ TỰ NHIÊN (số tự nhiên được định nghĩa là các số nguyên không âm)</span>
                    </div>`;
  }else{
    n=n*1;
    for (var i=1; i<=n; i++){
        if(n%i==0){
            arr.push(i);
        };
    };
    document.getElementById("timUoc").innerHTML = `${arr.join()}`
  }
};
// end BT4

// // start BT5
let daoNguoc=()=>{
    let str=document.getElementById("n5").value;
    let contentHTML=""
    let regex = /^\d+$/;
    if (regex.test(str)){
      var str2=str*1;      
      if(str2>0){
        var arr=[...str];
        for(var i=arr.length-1; i>=0; i--){
            contentHTML+=arr[i];
        }
      }else{
        contentHTML=`Vui lòng nhập số nguyên dương`}
    }else{
      contentHTML=`Vui lòng nhập số nguyên dương`
    }
    document.getElementById("daoNguoc").innerHTML=contentHTML;
}
// // end BT5

// start BT6
let timX = () => {
  var S = 0;
  var i = 1;
  while (S + i <= 100) {
    S = S + i;
    i++;
  }
  if (S + i > 100) {
    document.getElementById("timX").innerHTML = i - 1;
  } else {
    document.getElementById("timX").innerHTML = i;
  }
};
// end BT6

// start BT7
let bangCuuChuong = () => {
  let regex = /^-?[0-9]\d*(\.\d+)?$/
  contentHTML = "";
  var n = document.getElementById("n7").value;
  if(regex.test(n)) {
  for (var i = 0; i <= 10; i++) {
    contentHTML = contentHTML + `${n} x ${i} = ${n * i} <br/>`;
  }}else{
    alert('Định dạng không hợp lệ')
  }
  document.getElementById("bangCuuChuong").innerHTML = contentHTML;
};
// end BT7

// start BT8
let chiaBai = () => {
  contentHTML = "";
  var players = [ [], [], [], [] ];
  var cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S",
                "AS", "7H", "9K", "10D"]
  for( var i=0; i<=3; i++){
      players[i].push(cards[i],cards[i+4],cards[i+8]);  
      contentHTML=contentHTML+`<div>Player ${i+1}: ["${players[i][0]}"],["${players[i][1]}"],["${players[i][2]}"]</div>`
  }
  document.getElementById("chiaBai").innerHTML = contentHTML;
};
// end BT8

// start BT9
let timGaCho=()=>{
   let m=document.getElementById('m9').value*1;
   let n=document.getElementById('n9').value*1;
   if(m>n){
    alert('Tổng số con phải BÉ HƠN tổng số chân (m<n)')
   }else{
    var soCho=(n/2)-m;
    var soGa=m-soCho;
    document.getElementById('timGaCho').innerHTML=`<div class="text-primary">Số gà = ${soGa}</div><div class="text-success">Số chó = ${soCho}</div>`
   }
}
// end BT9

// start BT10
let gocLech = ()=>{
    var h=document.getElementById('h').value*1;
    if (h==12){h=0}
    var p=document.getElementById('p').value*1;
    if (h<0 || h>12 || p<0 || p>60){
      alert('Số giờ từ 1->12 và số phút từ 0->60')
    }else{
      var goc=Math.abs(0.5*h*60-(6*p-0.5*p));
      console.log(goc);
    }
    document.getElementById('gocLech').innerHTML=`${goc} độ`;
}
// end BT10
